package domain

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewQuestion(t *testing.T) {

	assert := assert.New(t)
	expect := &Question{
		Question: "What is 2+2?",
		Answer:   "4",
	}

	questionData := []string{"What is 2+2?", "4"}
	actual, err := NewQuestion(questionData)

	if err != nil {
		t.Fail()
		t.Logf("Test failed with error: %e", err)
	}

	assert.EqualValues(expect, actual, "The structs should have equals values")

}

func TestNewQuestionWrongLengthInput(t *testing.T) {

	assert := assert.New(t)
	expect := errors.New("question format is not valid")

	_, err := NewQuestion([]string{"Bad", "Format", "For", "Question"})

	assert.Equal(expect, err, "The function should throw error: %e", expect)

}

func TestAnswerQuestionSuccess(t *testing.T) {

	assert := assert.New(t)
	question, _ := NewQuestion([]string{"What is 2+2?", "4"})
	expected := Correct
	question.AnswerQuestion("4")
	actual := question.QuestionStatus
	assert.Equal(expected, actual, "the answer should result to true")
}

func TestAnswerQuestionFail(t *testing.T) {

	assert := assert.New(t)
	question, _ := NewQuestion([]string{"What is 2+2?", "4"})
	expected := InCorrect
	question.AnswerQuestion("5")
	actual := question.QuestionStatus

	assert.Equal(expected, actual, "the answer should result to false")
}
