package domain

import (
	"errors"
	"strings"
)

type QuestionStatus int

const (
	NA = iota
	Correct
	InCorrect
)

type Question struct {
	Question       string
	Answer         string
	QuestionStatus int
}

func NewQuestion(questionData []string) (*Question, error) {

	if len(questionData) != 2 {
		return nil, errors.New("question format is not valid")
	}

	return &Question{
		Question:       questionData[0],
		Answer:         questionData[1],
		QuestionStatus: NA,
	}, nil
}

func (question *Question) AnswerQuestion(answer string) {

	if question.Answer != strings.TrimSpace(answer) {
		question.QuestionStatus = InCorrect
	} else {
		question.QuestionStatus = Correct
	}
}
