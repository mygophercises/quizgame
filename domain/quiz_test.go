package domain

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func generateSimpleQuizData() []*Question {
	questionData := []string{"What is 2+2?", "4"}
	var questions []*Question
	for i := 0; i < 10; i++ {
		question, _ := NewQuestion(questionData)
		questions = append(questions, question)
	}
	return questions
}

func TestStandardNewQuiz(t *testing.T) {
	assert := assert.New(t)
	questions := generateSimpleQuizData()
	expected := &Quiz{
		Questions: questions,
	}
	actual, err := NewQuiz(questions)

	if err != nil {
		t.Fail()
		t.Logf("Test failed with error: %e", err)
	}

	assert.EqualValues(expected, actual, "The structs data should be equal")
}

func TestQuizResult(t *testing.T) {
	assert := assert.New(t)

	questions := generateSimpleQuizData()

	quiz, err := NewQuiz(questions)

	if err != nil {
		t.Fail()
		t.Logf("Test failed with error: %e", err)
	}
	var expected []QuestionStatus
	for _, question := range quiz.Questions {
		question.AnswerQuestion("4")
		expected = append(expected, Correct)
	}
	actual := quiz.GetResults()

	assert.EqualValues(expected, actual, "Should contain array with Correct as question status")

}
