package domain

type Quiz struct {
	Questions []*Question
}

func NewQuiz(questions []*Question) (*Quiz, error) {
	return &Quiz{
		Questions: questions,
	}, nil
}

func (quiz *Quiz) GetResults() []QuestionStatus {
	var questionsResults []QuestionStatus

	for _, question := range quiz.Questions {
		questionsResults = append(questionsResults, QuestionStatus(question.QuestionStatus))
	}

	return questionsResults
}
