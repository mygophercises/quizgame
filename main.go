package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/gophercises/quizgame/adapters"
	"gitlab.com/gophercises/quizgame/domain"
)

func loadQuiz(arg []string) *domain.Quiz {

	flag.Parse()
	var csvFile string
	if len(arg) < 2 {
		csvFile = "problems.csv"
	} else {
		csvFile = os.Args[1]
	}
	questions := adapters.NewCsvAdapter().FetchQuestions(csvFile)
	quiz, err := domain.NewQuiz(questions)
	if err != nil {
		panic(err)
	}

	return quiz
}

func playerInput(answerCh chan string) {
	reader := bufio.NewReader(os.Stdin)
	playerAnswer, err := reader.ReadString('\n')
	if err != nil {
		log.Fatal(err)
	}
	answerCh <- playerAnswer
}

func main() {
	quiz := loadQuiz(os.Args)
	timer := time.NewTimer(5 * time.Second)

	for _, question := range quiz.Questions {
		fmt.Println(question.Question)

		answerCh := make(chan string)
		go playerInput(answerCh)

		select {
		case <-timer.C:
			result := make(map[domain.QuestionStatus]int)
			for _, answer := range quiz.GetResults() {
				result[answer] = result[answer] + 1
			}
			fmt.Printf("Correct/Not Correct/Not Answered: %d, %d, %d", result[domain.Correct], result[domain.InCorrect], result[domain.NA])
			return
		case answer := <-answerCh:
			question.AnswerQuestion(answer)
		}
	}
}
