package interfaces

import "gitlab.com/gophercises/quizgame/domain"

type QuizData interface {
	FetchQuestions(source string) []*domain.Question
}
