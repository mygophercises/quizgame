package adapters

import (
	"encoding/csv"
	"io"
	"log"
	"os"

	"gitlab.com/gophercises/quizgame/domain"
	"gitlab.com/gophercises/quizgame/interfaces"
)

type csvAdapter struct{}

func NewCsvAdapter() interfaces.QuizData {

	return &csvAdapter{}

}

func (csvAdapter *csvAdapter) FetchQuestions(source string) []*domain.Question {
	if source == "" {
		source = "problems.csv"
	}

	file, err := os.Open(source)
	if err != nil {
		log.Fatal(err)
	}
	r := csv.NewReader(file)

	var questions []*domain.Question

	for {
		record, err := r.Read()

		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatal(err)
		}
		question, err := domain.NewQuestion(record)
		if err != nil {
			log.Fatal(err)
		}
		questions = append(questions, question)
	}

	return questions

}
